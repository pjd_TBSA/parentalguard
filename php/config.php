<?php

$DBHOST="localhost";
$DBUSER="root";
$DBPASS="root";
$DBBASE="swisspatrol";

$_IPCHECK=$_SERVER['REMOTE_ADDR'];

$_IPCHECK="188.154.152.146";

$_DAYS=10; // DAYS OF ACTIVITY;

$_COLOR=array("lightsalmon","salmon","darksalmon","lightcoral","indianred",
              "crimson","firebrick","red","darknet",
              "coral","tomato","orangered",
              "gold","orange","darkorange","darkkhari","yellow","forestgreen",
              "green","darkgreen","yellowgreen","seagreen","darkolivegreen",
              "darkcyan","cadetblue","teal","cyan","powderblue","blue",
              "dodgerblue","steelblue","mediumblue","darkslateblue");

$_API_GOOGLE="AIzaSyBb2MAdnNpYADFI4aDqexbJRwimzNgwF2U";

$_SOCIALNETWORK_TO_SEARCH=array("facebook"=>1,
                                "twitter"=>1,
                                "tiktok"=>1,
                                "instagram"=>1,
                                "pinterest"=>1,
                                "whatsapp"=>1,
                                "qq"=>0,
                                "weChat"=>1,
                                "qzone"=>0,
                                "tumblr"=>1,
                                "skype"=>1,
                                "viber"=>1,
                                "line"=>0,
                                "snapchat"=>1,
                                "vkontakte"=>1,
                                "linkedin"=>1,
                                "telegram"=>1,
                                "reddit"=>1,
                                "taringa"=>1,
                                "foursquare"=>1,
                                "renren"=>1,
                                "tagged"=>1,
                                "badoo"=>1,
                                "tinder"=>1,
                                "myspace"=>1,
                                "stumbleupon"=>1,
                                "thedots"=>1,
                                "kiwibox"=>1,
                                "skyrock"=>1,
                                "delicious"=>1,
                                "snapfish"=>1,
                                "reverbnation"=>1,
                                "wayn"=>1,
                                "flixster"=>1,
                                "care2"=>1,
                                "cafemom"=>1,
                                "ravelry"=>1,
                                "nextdoor"=>1,
                                "cellufun"=>1,
                                "youtube"=>1,
                                "vine"=>1,
                                "classmates"=>1,
                                "myheritage"=>1,
                                "viadeo"=>1,
                                "xing"=>1,
                                "xanga"=>1,
                                "livejournal"=>1,
                                "friendster"=>1,
                                "funnyordie"=>1,
                                "gaiaonline"=>1,
                                "weheartit"=>1,
                                "buzznet"=>1,
                                "flickr"=>1,
                                "meetup"=>1,
                                "meetic"=>1,
                                "tout"=>1,
                                "mixi"=>1,
                                "douban"=>1,
                                "vero"=>1,
                                "quora"=>1,
                                "discord"=>1,
                                "spreely"=>1,
                                "netflix"=>1);

$_OTHER_ACTIVITY=array("safebrowsing.googleapis.com"=>"Safe Google Search",
                       "bing.com"=>"Bing utilisé",
                       "api.mapbox.com"=>"Geoloc activé",
                       "gitlab"=>"Developpeur à bord",
                       "avast"=>"Avast Antivirus",
                       "amazonaws"=>"Amazon Used",
                       "hiventy.io"=>"Traduction",
                       "mail.google.com"=>"Gmail",
                       "wpad.home"=>"Tablette ON");
?>
